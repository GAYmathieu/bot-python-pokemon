import discord
from discord.ext import commands
import requests
import random

# config bot
intents = discord.Intents.default()
intents.guilds = True
intents.message_content = True
bot = commands.Bot(command_prefix='!', intents=intents)


random.seed()

# get details from API Pokemon
def get_pokemon_details(pokemon_id, shiny=False):
    url = f'https://pokeapi.co/api/v2/pokemon/{pokemon_id}/'
    response = requests.get(url)

    if response.status_code == 200:
        pokemon_details = response.json()

        # change pokemon to shiny 
        if shiny:
            pokemon_details['sprites']['front_default'] = get_shiny_sprite_url(pokemon_id)

        return pokemon_details
    else:
        return None
# get evolution details
def get_evolution_details(pokemon_id):
    url = f'https://pokeapi.co/api/v2/evolution-chain/{pokemon_id}/'
    response = requests.get(url)

    if response.status_code == 200:
        return response.json()
    else:
        return None
    
def get_shiny_sprite_url(pokemon_id):
    return f'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/{pokemon_id}.png'

captured_pokemons = {}



## commands

# start encounter with pokemon
@bot.command()
async def enc(ctx):
    # generate a random number for shiny pokemon
    has_shiny = random.random() < 0.1 

    # get 3 wild pokemon
    wild_pokemons = [get_pokemon_details(str(random.randint(1, 807)), shiny=has_shiny) for _ in range(3)]

    pokemon_images = []

    # wild pokemon details
    for index, wild_pokemon in enumerate(wild_pokemons, start=1):
        name = wild_pokemon['name']
        types = ', '.join(type_data['type']['name'] for type_data in wild_pokemon['types'])
        image_url = wild_pokemon['sprites']['front_default']

        pokemon_images.append(image_url)

        embed = discord.Embed(title=f"Wild Pokemon {index}: {name.capitalize()}", color=0x00ff00)
        embed.add_field(name="Name", value=name.capitalize(), inline=False)
        embed.add_field(name="Types", value=types, inline=False)
        embed.set_image(url=image_url)

        await ctx.send(embed=embed)

        # print a special message
        if has_shiny and random.random(): 
            await ctx.send("🌟 Shiny Pokemon ! 🌟")

    # add reaction below message to pick a pokemon
    for index in range(1, 4):
        await ctx.message.add_reaction(f"{index}\u20e3")

    # check if user has pick a pokemon
    def check(reaction, user):
        return user == ctx.author and str(reaction.emoji) in [f"{i}\u20e3" for i in range(1, 4)]

    try:
        reaction, _ = await bot.wait_for('reaction_add', timeout=20.0, check=check)
        chosen_pokemon = wild_pokemons[int(reaction.emoji[0]) - 1]

        # display the chosen pokemon
        embed = discord.Embed(title=f"You've chosen {chosen_pokemon['name'].capitalize()} !", color=0x00ff00)
        types = ', '.join(type_data['type']['name'] for type_data in chosen_pokemon['types'])
        embed.add_field(name="Espèce", value=chosen_pokemon['name'].capitalize(), inline=False)
        embed.add_field(name="Types", value=types, inline=False)
        embed.set_thumbnail(url=chosen_pokemon['sprites']['front_default'])

        await ctx.send(embed=embed)

        # start catching the pokemon
        capture_attempts = 3  # 3 tries

        for attempt in range(1, capture_attempts + 1):
            roll = random.randint(1, 6)
            await ctx.send(f"**Attempt to capture {attempt}/{capture_attempts}:** You throw a pokeball... Result : {roll}")

            if roll == 6:
                await ctx.send("Congrats ! you captured the pokemon !")

                # add the pokemon to the dico
                captured_pokemons[chosen_pokemon['name'].capitalize()] = chosen_pokemon

                break

        else:
            await ctx.send("The wild pokemon has escaped")

    except TimeoutError:
        await ctx.send("Timeout. The wild pokemon has escaped")

# display the pokemon list
@bot.command()
async def captured(ctx):
    if captured_pokemons:
        embed = discord.Embed(title="Your pokemon", color=0x0000ff)

        for name, details in captured_pokemons.items():
            types = ', '.join(type_data['type']['name'] for type_data in details['types'])
            embed.add_field(name=name, value=f"**Types:** {types}", inline=False)

        await ctx.send(embed=embed)
    else:
        await ctx.send("You don't have any pokemon for now")


#show pokemon info
@bot.command()
async def info(ctx, *, pokemon_name):
    # check if the pokemon has been catch
    if pokemon_name.capitalize() in captured_pokemons:
        pokemon_details = captured_pokemons[pokemon_name.capitalize()]

        embed = discord.Embed(title=f"Info about {pokemon_name.capitalize()}", color=0xffd700)
        types = ', '.join(type_data['type']['name'] for type_data in pokemon_details['types'])
        embed.add_field(name="Name", value=pokemon_name.capitalize(), inline=False)
        embed.add_field(name="Types", value=types, inline=False)
        embed.add_field(name="Pokédex", value=pokemon_details.get('pokedex_description', 'No description'), inline=False)
        embed.set_thumbnail(url=pokemon_details['sprites']['front_default'])
        
        pokemondb_url = f'https://pokemondb.net/pokedex/{pokemon_name.lower()}'
        embed.add_field(name="More info", value=f"[Pokédex info]({pokemondb_url})", inline=False)

        await ctx.send(embed=embed)
    else:
        await ctx.send(f"You don't have any pokemon named {pokemon_name.capitalize()}.")
  
  
#display command list
@bot.command()
async def commands(ctx):
    commands_list = [
        "**!enc** - Simulate an encounter with three wild Pokémon.",
        "**!captured** - Display the list of captured Pokémon.",
        "**!info [pokemon_name]** - Show information about a captured Pokémon.",
        "**!evolve [pokemon_name]** - Evolve a captured Pokémon.",
        "**!commands** - Display all available commands."
    ]

    embed = discord.Embed(title="Available commands", color=0x3498db)
    embed.description = "\n".join(commands_list)
    await ctx.send(embed=embed)
    

# evolution command  
@bot.command()
async def evolve(ctx, *, pokemon_name):

    if pokemon_name.capitalize() in captured_pokemons:
        pokemon_details = captured_pokemons[pokemon_name.capitalize()]

        # check if pokemon has evolution
        if 'id' in pokemon_details and int(pokemon_details['id']) < 807:
            next_pokemon_id = str(int(pokemon_details['id']) + 1)
            evolved_pokemon_details = get_pokemon_details(next_pokemon_id)

            # update the pokemon list
            if evolved_pokemon_details:
                del captured_pokemons[pokemon_name.capitalize()] 
                captured_pokemons[evolved_pokemon_details['name'].capitalize()] = evolved_pokemon_details

                await ctx.send(f"{pokemon_name.capitalize()} has evolved in {evolved_pokemon_details['name'].capitalize()} !")

                await info(ctx, pokemon_name=evolved_pokemon_details['name'])
            else:
                await ctx.send(f"Can't get evolve info for {pokemon_name.capitalize()}.")
        else:
            await ctx.send(f"{pokemon_name.capitalize()} doesn't have evolution")
    else:
        await ctx.send(f"You don't have any pokemon named {pokemon_name.capitalize()}.")
        
         
bot.run('Your_Token')
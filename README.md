# Bot Pokemon Python


## Installation

1. Clonez le dépôt

3. Installez les packages Python requis :

    ```bash
    pip install -r requirements.txt
    ```

4. Configurez le Bot Discord :

    - Créez un nouveau bot sur le [Portail des développeurs Discord](https://discord.com/developers/applications).
    - Copiez le jeton du bot.
    - Remplacez `'Your_Token'` à la dernière ligne du script par votre jeton de bot réel.

## Utilisation

Exécutez le script du bot :

```bash
python BotPokemon.py
```

## Commandes

### Commandes du Bot Pokémon

Ci-dessous se trouve la liste des commandes disponibles dans le bot Pokémon :

- **!enc** : Simuler une rencontre avec trois Pokémon sauvages.

- **!captured** : Afficher la liste des Pokémon capturés.

- **!info [nom_pokemon]** : Afficher des informations sur un Pokémon capturé.

- **!evolve [nom_pokemon]** : Faire évoluer un Pokémon capturé.

- **!commands** : Afficher toutes les commandes disponibles.

N'hésitez pas à utiliser ces commandes pour interagir avec le bot Pokémon et explorer le monde passionnant des Pokémon !



## Axes d'amélioration

- Faire en sorte qu'un pokémon ne puisse pas évoler lorsqu'il atteint sa forme finale.
- Modifier l'apparation de pokémon shiny. Afin qu'un seul pokémon chromatique puisse apparaitre de temps en temps.


